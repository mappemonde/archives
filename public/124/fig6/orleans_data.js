const layers = [
  { title: '1946', mapId: 'year-1946' },
  { title: '1954', mapId: 'year-1954' },
  { title: '1962', mapId: 'year-1962' },
  { title: '1968', mapId: 'year-1968' },
  { title: '1975', mapId: 'year-1975' },
  { title: '1982', mapId: 'year-1982' },
  { title: '1990', mapId: 'year-1990' },
  { title: '1999', mapId: 'year-1999' },
  { title: '2008', mapId: 'year-2008' },
  { title: '2013', mapId: 'year-2013' }
];