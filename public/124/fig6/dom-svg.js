var container = document.querySelector('.container');
var controls = container.querySelectorAll('.control i');
var nameControls = container.querySelectorAll('span.control');
var year = container.querySelector('.year');
var back = container.querySelector('.back');
var forward = container.querySelector('.forward');
var noms = container.querySelector('#noms');
var blank = container.querySelector('rect');

if (NodeList.prototype.forEach === undefined) {
  NodeList.prototype.forEach = function (callback) {
  	[].forEach.call(this, callback);
  }
}

var getLayerIndex = function (title) {
  return layers
    .map(function (layer) { return layer.title; })
	.indexOf(title);
};

var getLayer = function (index) {
  return layers[index];
};

var selectLayer = function (index) {
  document.querySelectorAll('.active').forEach(function (item) {
	  item.classList.remove('active');
  });
  back.classList.add('active');
  forward.classList.add('active');

  switch (index) {
  	case -1:
      back.classList.remove('active');
      break;
  	case layers.length - 1:
  	  forward.classList.remove('active');
  	  break;
  }

  if (index !== -1) {
  	var layer = getLayer(index);
    setYear(layer.title);
    blank.classList.add('show');
    document.querySelector('#' + layer.mapId).classList.add('active');
  } else {
  	setYear('...');
    blank.classList.remove('show');
  }
};

var setYear = function (title) {
	year.textContent = title;
};

controls.forEach(function (control) {
  control.addEventListener('click', function (e) {
	var index = getLayerIndex(year.textContent);
	index = index === -1
	  ? 0
	  : this.classList.contains('back')
	    ? --index
		: ++index;
    selectLayer(index);
  });
});

nameControls.forEach(function (control) {
  control.addEventListener('click', function (e) {
    nameControls.forEach(function (item) {
      item.classList.contains('show') ? item.classList.remove('show') : item.classList.add('show');
    });
    noms.classList.contains('show') ? noms.classList.remove('show') : noms.classList.add('show');
  });
});