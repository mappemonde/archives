
// -------------------------------- DEBUT FONCTION CLICK ----------------------------------------	

// Initialisation de la div des informations : invite à cliquer un pays
function infoPaysInit (){	
	
	var texteInit ="<p><b><h6>Cliquez sur un pays</h6></b></p>"; // Texte à écrire
	
	document.getElementById("Infos").innerHTML = texteInit; // Affichage du texte en html		
}
// -------------------------------- FIN FONCTION CLICK ------------------------------------------	


// -------------------------------- DEBUT FONCTION INFO PAYS ------------------------------------	

// Fonction qui écrit les informations générales du pays
function infoPays(dataset){
	
	// Filtrer pour le pays sélectionné
	var data = dataset.filter (function (d){
					return ((d.pays == pays_courant)&&(d.date == dateCourante));
					});	
	
	// Récupérer les valeurs du dataset
	var nom = data[0].nom_complet;
	var sup = data[0].sup;
	var etat = data[0].etat;
	var lang = data[0].langue;
	var pop = data[0].pop;
	var arr = data[0].arr_brut;
	var dep = data[0].dep_brut;
	
	// Image
		
	// On définit la source de l'image
	srcImg = "drapeaux/"+pays_courant+".png";
	IMG = '<img src="'+srcImg+'" width="62.5" height="37.5" />';

	// Variable contenant le texte html
	var texte = "<span style='float:right'>"+IMG+"</span><span><h6>"+nom+"</h6></span>"+
			"<p><h5><b>Superficie :</b> "+sup+"<br>"+
			"<b>Forme de l'état :</b> "+etat+"<br />"+
			"<b>Langue officielle :</b> "+lang+"<br />"+
			"<b>Population en "+dateCourante+":</b> "+pop+" Millions d'habitants";

	//gestion des valeurs nulles du dataset
	if ((dep == null)&&(arr == null)){//si les 2 ne sont pas renseignées
		texte += "<br><b>Nombre de touristes en "+dateCourante+" :</b> n/a<br />"+
						"<b>Dépense totale en "+dateCourante+" :</b> n/a</h5></p>";
	}else if (dep == null){//si la valeur brute de la dépense n'est pas renseignée
		texte += "<br><b>Nombre de touristes en "+dateCourante+" :</b> "+arr+" 000<br />"+
						"<b>Dépense totale en "+dateCourante+" :</b> n/a</h5></p>";
	}else if (arr == null){//si les 2 ne sont pas renseignées
		texte += "<br><b>Nombre de touristes en "+dateCourante+" :</b> n/a<br />"+
						"<b>Dépense totale en "+dateCourante+" :</b>"+ dep+" M$</h5></p>";
	}else{//si les deux sont renseignées
		texte += "<br><b>Nombre de touristes en "+dateCourante+" :</b> "+arr+" 000<br />"+
						"<b>Dépense totale en "+dateCourante+" :</b>"+ dep+" M$</h5></p>";
	} //fin boucles
	
	// Conteneur
	var div = document.getElementById("Infos");
	
	// Ecriture dans la div les infos de l'index
	div.innerHTML = texte;

	}
// -------------------------------- FIN FONCTION INFO -------------------------------------------	