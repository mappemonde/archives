function init() {
	// Initilisation histogramme continent (valeurs initiales)
	dateCourante = 1996;
	pays_courant = "AMS";

	d3.json('DATA.json', function (dataset) {
		this.donnees_stat = dataset;
		map(dateCourante); // Fonction du script Carte
		drawHistoAMS(dataset); // Fonction du script Histo_continent
	});
				
	// Onitialisation de la bibliothèque dragit dans la div concernée avec les valeurs du dataset
	dragit.init("sliderplace");
	dragit.time = {min:0, max:17, step:1, current:0}

	// Lance la fonction update lors d'un évènement
	dragit.evt.register("update", update);

	// Au clic modification de la valeur du slider sélectionné par son ID
	d3.select("#slider-time").property("value", dragit.time.current);
	d3.select("#slider-time")
	.on("click", function() { 
		update(parseInt(this.value), 500);
	});
}

function update(v, duration) {
	// Met à jour la valeur du slider et lance la fonction displayYear lors d'un clic
	dragit.time.current = v;
	displayYear(1996 + dragit.time.current);
	d3.select("#slider-time").property("value", dragit.time.current);
}
  
function displayYear(year) {
	// Met à jour la variable gloable dateCourante
	dateCourante = dragit.time.min + Math.round(year);

	// Met à jour l'histogramme continent
	d3.select("#grapheAMS").remove();
	d3.json('DATA.json', drawHistoAMS);
	
	// Met à jour la carte
	d3.select("#map").remove();
	map(dateCourante);
	
	// Met à jour l'histogramme pays
	if (pays_courant != "AMS"){ // Si un pays est sélectionné
		d3.select("#graphe").remove();
		d3.json('DATA.json', drawHisto);
	}

	// Met à jour les infos
	if (pays_courant != "AMS"){ // Si un pays est sélectionné
		d3.select("#infoPays").remove();
		d3.json('DATA.json', infoPays);
	}
}