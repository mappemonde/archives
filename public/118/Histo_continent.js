function drawHistoAMS (dataset) {

	// Sélection du pays pays courant (variable globale)								
	var data = dataset.filter (function (d){
		return d.pays == "AMS";}
	);	

					
	//----------------------------------------------- DEBUT DES CONSTANTES --------------------------------------------------

	// Les constantes du jeu de données et de l'écriture HTML sont assignées à des variables

	// Largeur et hauteur du graphe
	var larg = 700;
	var haut = 200;
		
	// Marges
	var margleft = 35;
	var margright = 35;
	var margtop = 20;
	var margbot = 20;
		
	// Ecartement des barres
	var barPadding = 2; 
		
	// Valeurs issues du dataset
	var nbb = data.length; // Nombre de barres : nombre d'enregistrement dans le dataset		
	var maxevo = d3.max(data, function (d) {
		return d.evolution;
	}); // Valeur max évolution
	var minevo = d3.min(data, function (d) {
		return d.evolution;
	}); // Valeur min évolution
	var maxabsevo = Math.max(Math.abs(maxevo),Math.abs(minevo)); // Valeur absolue maximum
	var maxdep = d3.max(data, function (d) {
		return d.dep;
	}); // Valeur max dépense
	var maxdate = d3.max(data, function (d) {
		return d.date;
	}); // Date max
	var mindate = d3.min(data, function (d) {
		return d.date;
	}); // Date min
											
	// Coefficient hauteur des barres et légende									
	var ch = ((haut - margtop - margbot) / (2*maxabsevo)); // Place pour les barres / 2*max (positif et négatif)

	// Largeur  maximum possible des barres 
	var lb = ((larg-margleft-margright-(nbb)*barPadding)/ nbb);//place dispo - nombre de barres * espacement / nombre barres

	// ------------------------------------------------ FIN CONSTANTES ----------------------------------------------------------


	//------------------------------------------------- DEBUT DES AXES ----------------------------------------------------------		
	
	// Création de l'élément SVG 
	var svg = d3.select("#Histo_continent")//sélection de la div qui
	.append("svg")
	.attr("id","grapheAMS")
	.attr("width", larg)
	.attr("height", haut);


	// Axe x : dates
	var x = d3.scale.linear()
	.domain([mindate-1,maxdate])
	.range([margleft, (larg-margright)]);
		
	var xAxis = d3.svg.axis()
	.scale(x)
	.ticks(data.length)//nombre de valeur dans le dataset / 2
	.tickFormat(d3.format("d"))
	.orient("bottom");

	// Axe y : histogrammes évolution
	var y = d3.scale.linear()
	.domain([-maxabsevo/100,maxabsevo/100])//max+1 (en %) pour avoir la même échelle que les données
	.range([haut-margbot, margtop]);

	var yAxis = d3.svg.axis()
	.scale(y)
	.ticks(10)
	.tickFormat(d3.format("%"))
	.orient("left") ;

	// Axe y2 : dépense courbe
	var y2 = d3.scale.linear()
	.domain([0, (maxdep+1)])//pour avoir la même échelle que les données
	.range([y(0), margtop]);

	var yAxis2 = d3.svg.axis()
	.scale(y2)
	.ticks(5)
	.tickFormat(d3.format("d"))
	.orient("right");

	// Afficher axes 

	// Afficher x
	svg.append("g")
	.attr("class", "x axis")
	.attr("transform", "translate(0," + (y(0)+maxabsevo*ch)+ ")")
	.attr ("font-size", "10px")
	.call(xAxis);

	// Afficher y
	svg.append("g")
	.attr("class", "y axis")
	.attr("transform", "translate("+margleft+",0)" )
	.attr ("font-size", "10px")
	.call(yAxis)
	.append("text")
	.attr ("x", 10-margleft)
	.attr("y", margtop-10)
	.style("text-anchor", "start")
	.attr ("font-size", "12px")
	.attr ("font-weight", "bold")
	.attr("fill", "black")
	.text("Pourcentage");

	// Afficher y2
	svg.append("g")
	.attr("class", "y axis")
	.attr("transform", "translate("+ (larg -margright) +",0)" )
	.attr ("font-size", "10px")
	.attr("fill",couleurs[2])
	.call(yAxis2)
	.append("text")
	.attr ("x", margright-5)
	.attr("y", 10)
	.style("text-anchor", "end")
	.attr ("font-size", "12px")
	.attr ("font-weight", "bold")
	.attr("fill", "black")
	.text("$/touriste");

	// -------------------------------------------------------- FIN AXES ----------------------------------------------------------


	// -------------------------------------------------------- DEBUT HISTOGRAMMES ------------------------------------------------	

	var choix_couleur = function(valeur) {
		for (i=nbClasses; i>=1; i--){ //boucle sur les classes (en sens inverse pour peupler les valeurs)
			if (valeur>=classes[i-1]){ //teste si la valeur est dans la classe
				return couleurs[i]; //Renvoie la couleur correspondante
			}
		}
	};

	// Histogramme
	svg.selectAll("rectAMS")
	.data(data)
	.enter()
	.append("rect")
	.attr("width", lb) // Largeur des barres
	.attr("height", function(d) { // Hauteur des barres
		if(d.evolution <0){
			return (-d.evolution*ch);	
		}else{
			return d.evolution*ch;
		}
	})
	.attr("x", function(d, i) { // Abcisse des barres fonction de l'indice du dataset
		return (margleft + i * (lb+barPadding)); 
	})
	.attr("y", function(d) { // Ordonnée des barres
		if (d.evolution <0){
			return (y(0));
		}else{
			return (y(0)-d.evolution*ch); 
		}
	})

	// Couleur histogramme <0 et >0
	.attr("fill", function (d) {
		return choix_couleur(d.evolution);
	});

	// Valeur de l'histogramme à la date courante (visible tout le temps)
	svg.selectAll("Histo_continent")
	.data(data)
	.enter()
	.append("text")
	.text(function (d){
		if (d.date==dateCourante) {
			return Math.round(d.evolution*10)/10 + " %"; // Arrondi à 1 chiffre après la virgule
		}
	})
	.attr("x",function(d, i) {
		return (margleft+lb/2 + (i * (lb+barPadding))); 
	})
	.attr("y", function (d){
		if (d.evolution ==0){
			return y(0);
		}else if (d.evolution<0){
			return (y(0)- d.evolution*ch + 12);
		}else{
			return (y(0) -d.evolution*ch-5);
		}
	})
	.style("text-anchor", "middle")
	.attr ("font-weight", "bold")
	.attr ("font-size", "11px")

	// Couleur idem histogramme
	.attr("fill", "grey");

	// -------------------------------------------------------- FIN HISTOGRAMMES ----------------------------------------------------------


	// -------------------------------------------------------- DEBUT COURBE ------------------------------------------------	

	// Filtrage des enregistrements où la valeur de la dépense est nulle
	data_filtre = data.filter(function(d){
		if (d.dep === 0){
			return false
		}
		return true;
	});

	// Création de la variable svg.line
	var line = d3.svg.line() 
	.interpolate("linear")
	.x(function(d,i) { 
		return ((d.date-mindate+1)*(lb+barPadding)+margleft);
	})
	.y(function(d) { 
		return y2(d.dep); 
	});

	// Affichage de la courbe
	svg.selectAll("path")
	.data(data_filtre)
	.enter()
	.append("path")
	.attr("class", "line")
	.attr("d", line(data_filtre)); 
		
	// Dot (point sur la courbe)
	svg.selectAll(".dot")
	.data(data)
	.enter().append("circle")
	.attr("class","dot")
	.attr("r",function (d){
		if (d.dep ==0){
			return 0;
		} else {
			return 3;
		}
	})
	.attr("cx", function(d) { return x(d.date); })
	.attr("cy", function(d) { return y2(d.dep); });

	// Affichage valeur de la date courante
	NbDate = dateCourante-mindate+1;

	xmarq = margleft+NbDate*(lb+barPadding); // Abcisse marqueur : marge + nombre de date depuis le depart*largeur histogramme

	yCourant = data_filtre.filter (function (d){
		return d.date == dateCourante;}
	);	

	yValeur = d3.max (yCourant, function (d){	// Max mais on veut la valeur (il n'y en a qu'une)
		return d.dep;
	});

	ymarq = y2(yValeur); // Application de l'échelle

	// Gestion des enregistrement où la dépense est nulle
	
	if (yCourant.length == 0){ // Si pas de données à la date courante

		// n/a
		svg.append("text")
		.attr ("x", xmarq)
		.attr("y", y2(maxdep)-5)
		.style("text-anchor", "middle")
		.attr ("font-family", "sans-serif")
		.attr ("font-style", "italic")
		.attr ("font-size", "11px")
		.attr ("font-weight", "bold")
		.attr("fill", couleurs[2])
		.text("n/a");

	} else { // Si il y a des données

		// Valeur marqueur
		svg.append("text")
		.attr ("x", xmarq)
		.attr("y", ymarq+12)
		.style("text-anchor", "middle")
		.attr ("font-family", "sans-serif")
		.attr ("font-size", "11px")
		.attr ("font-weight", "bold")
		.attr("fill", couleurs[2])
		.text("$ " + Math.round(yValeur) );

	}
	
	// -------------------------------------------------------- FIN COURBE ----------------------------------------------------------

	// -------------------------------------------------------- DEBUT POPUP ------------------------------------------------		

	//création des 3 popup grâce à la sous-bibliothèque d3-tip.js	

	// Popup évolution
	var tip = d3.tip()
	.attr('class', 'd3_tip')
	.offset(function (d) {
		if (d.evolution == null){
			return [y(0)-20,0];
		} else if(d.evolution==0){
			return [y(0)-17,0]
		} else if (d.evolution<0){
			return [y(0)-d.evolution*ch-5,0];
		} else if (d.evolution>0){
			return [y(0)-d.evolution*ch-20, 0];
		}
	})
	.html(function(d) {
		if (d.date == dateCourante) {
			return "";
		} else if (d.evolution == 0) {
			return "<strong><span style='font-size: 11px; color:black'>"+d.evolution+" %</strong></span>";
		} else if (d.evolution === null){
			return "<strong><span style='font-size: 11; color:black'>n/a</strong></span>";
		} else if (d.evolution<0) { 
			return " <strong><span style='font-size: 11px; color:grey'>" + Math.round(d.evolution*10)/10 + " %</strong></span>";
		} else if (d.evolution>0){
			return " <strong><span style='font-size: 11px; color:grey'>" + Math.round(d.evolution*10)/10 + " %</strong></span>";
		}
	});

	// Popup dépense
	var tipdot = d3.tip()
	.attr('class', 'd3_tip')
	.offset(function (d){
			if (d.dep == 0){
				return [y2(maxdep),lb/2];
			} else {
				return [y2(d.dep),lb/2];
			}
	})
	.html(function(d) {
		if (d.date == dateCourante) {
			return "";
		} else if (d.dep == 0){
			return " <strong><span style='font-size: 11px; color:grey'>n/a </strong></span>";
		} else {
			return " <strong><span style='font-size: 11px; color:grey'>$ " + Math.round(d.dep) + "</strong></span>";
		}
	})
	.style("text-anchor", "middle");

	// Popup date
	var tipdate = d3.tip()
	.attr('class', 'd3_tip')
	.offset([-20,0])
	.html(function(d) { return " <strong><span style='font-size: 11px; color:"+couleurs[3]+"'>"+(d.date-1) +"-"+d.date+"</strong></span>" ;});

	// Appel des variables de popup
	svg.call(tip);
	svg.call(tipdot);
	svg.call(tipdate);

	// Création de fonctions pour gérer l'affichage de plusieurs popup simultanément grâce aux méthodes show et hide
	function tipOver(data){	
		tipOut(data);
		tip.show(data);
		tipdot.show(data);
		tipdate.show(data);
	} // tipOver

	function tipOut(data){
		tip.hide(data);
		tipdot.hide(data);
		tipdate.hide(data);
	} // tipOut

	// Création d'histogrammes blancs pour le passage de la souris
	svg.selectAll("Histo_continent")
	.data(data)
	.enter()
	.append("rect")
	.attr("width", lb)
	.attr("height", 2*maxabsevo*ch)
	.attr("stroke", function (d) {
		if (d.date == dateCourante){
			return "#788785";
		}})
		.attr("x",function(d, i) {
			return (margleft + i * (lb+barPadding));
		})
		.attr("y", margtop)
		.attr("fill", "transparent")
		// Appel des fonction créées plus haut au passage de la souris
		.on('mouseover', function(d){ tipOver(d);})
		.on('mouseout', function(d){ tipOut(d);})
		.on('click', function(d,i){ // Mise à jour au clic
			tipOut(data);
			update(d.date - 1996);
		});
	};
//---------------------------------------------------------- FIN POPUP ------------------------------------------------------		
