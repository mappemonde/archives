// loader settings
var opts = {
  lines: 9, // The number of lines to draw
  length: 9, // The length of each line
  width: 5, // The line thickness
  radius: 14, // The radius of the inner circle
  color: '#EE3124', // #rgb or #rrggbb or array of colors
  speed: 1.9, // Rounds per second
  trail: 40, // Afterglow percentage
  className: 'spinner', // The CSS class to assign to the spinner
};

function map (date){
	//------------------------------------------------------DEBUT CONSTANTES----------------------------
	//Définition de variables globales concernant la date courante (en paramètre de la fonction)
	e_date="e_"+date;
	d_date="d_"+date;
	var d_evo;
	var d_dep;

	// Définition de la taille de la carte
	var width = 750;
	var height = 700;

	// On crée un tableau de couleurs que l'on pourra réutiliser lors de la création de notre carte choroplèthe
	//var couleurs = ['#FFFFFF','#2166ac','#67a9cf','#d1e5f0','#fddbc7','#ef8a62','#E83C1A','#b2182b','#949494'];
	couleurs = ['#FFFFFF','#1e9977','#7fb792','#c1d7af','#fef8cb','#e2b091','#c06a58','#981b1a','#949494'];

	//[blanc hors zone,-60 -30,-30 -9,-9 0,0 20,20 50,50 80,80 120,n/a]

	//On crée une variable qui va contenir les classes de la choroplèthe (croissantes !) ici 8 valeurs pour 7 classes (intervalles)
	classes = [-60,-30,-9,0,20,50,80,120];
	//nombre de classes (intervalles) ici 7
	nbClasses = classes.length-1;
	//taille (largeur) des classes (pour gérer l'affichage de la légende avec largeur des rectangles = largeur des classes)
	var largeurClasse = new Array();
	//remplissage du tableau de taille des classes
	for (i=0;i<nbClasses;i++){
		largeurClasse[i] = Math.abs(classes[i+1]-classes[i]); //valeur absolue de la différence des bornes de classes
	}
	//taille de classes cumulées
	var largClasseCumul = new Array();
	var cumul=0;
	for (i=0; i<nbClasses; i++){//boucles sur les classes
		cumul+= largeurClasse[i];//variable cumul
		largClasseCumul[i]= cumul-largeurClasse[i]; //cumul- largeur pour avoir l'abscisse à utiliser pour la légende
	}

	//-----------------------------------------------------FIN CONSTANTES-------------------------------

	//-----------------------------------------------------DEBUT SVG------------------------------------
	// On crée un nouvel objet path qui permet de manipuler les données géographiques
	var path = d3.geo.path();

	// On définit les propriétés de la projection à utiliser
	var projection = d3.geo.azimuthalEqualArea()
	.rotate([40,27,-6]) // On centre la carte sur l'Amérique du Sud
	.scale(470) // On définit l'échelle de zoom
	.translate([width / 2, height / 2]);
	path.projection(projection);

	// On crée un nouvel objet dans lequel les données seront mises
	var svg = d3.select('#Carte').append("svg")
	.attr("id","map")
	.attr("width", width)
	.attr("height", height);

	//---------------------------------FIN SVG----------------------------------------------------------

	// -------------------------------- DEBUT DE LA CHOROPLETHE ----------------------------------------		  

	// Attente de chargement geojson
	var target = document.getElementById('spinner')
	var spinner = new Spinner(opts).spin(target);
	
	var dessinCh = function(geojson) {
		// Nouvelle variable comprenant les propriétés du svg  
		var continent = svg
		.append("g")
		.attr("code", "pays");

		// Nouvelle variable comprenant les propriétés de la variable continent 
		var features = continent
		.selectAll("path")
		.data(geojson.features);
	
		// Création de la carte choroplèthe grâce à une boucle for sur le tableau de classes

		features.enter()
		.append("path")
		.attr('class', 'pays')
		.attr('fill', function(d) {
			d_evo = donnees_stat.filter(function (d2){ if(d.properties.Code_pays === d2.pays) return d2;});
			if (d_evo[(date-1996)].evolution === null) {
				return couleurs[8];// Classe n/a (pas de données)
			} else {
				for (i=nbClasses; i>=1; i--){// boucle sur les classes (en sens inverse pour peupler les valeurs)
					if (d_evo[(date-1996)].evolution>=classes[i-1]){//test si la valeur est dans la classe en commençant par la 2eme)
						return couleurs[i];//Renvoie la couleur correspondante (
						}
					}
				}
			})
			.attr("d", path); // Affiche la carte
	};

	// -------------------------------- FIN DE LA CHOROPLETE ----------------------------------------	

	// -------------------------------- DEBUT DES PAYS HORS ZONE -------------------------------------		  
	// Fonction permettant de créer les pays non traités dans l'étude mais situant ceux qui le sont
	var dessinPHZ = function(geojson) {
		// Nouvelle variable pour les infobulles
		var tip = d3.tip() 
		.attr('class', 'd3-tip')
		.offset([-10, 0])
		.html(function(d) {
			return "<span class='t_commune'>"
			+d.properties.Nom_pays // Retourne le nom du pays
			+"<br><i>n/a</i>" // Retourne la mention n/a
		})
		svg.call(tip);
	
		// Nouvelle variable ayant les propriétés de svg  
		var continent2 = svg
		.append("g")
		.attr("code", "pays_hors_zone");

		// Nouvelle variable ayant les propriétés de continent2
		var features2 = continent2
		.selectAll("path")
		.data(geojson.features);

		// Création de la carte des pays vide
		features2.enter()
		.append("path")
		.attr('class', 'pays_hors_zone')
		.attr('fill', couleurs[0])
		.on('mouseover', function(d) { // Permet au survol de l'entité, d'afficher des données
			tip.show(d);
		})
		.on('mouseout',  function(d) {
			tip.hide(d);
		})
		.attr("d", path); 
	};
	
	// -------------------------------- FIN DES PAYS HORS ZONE --------------------------------------	

	// -------------------------------- DEBUT DES SYMBOLES ------------------------------------------	
	// Variable permettant de faire des symboles proportionnels
	var radius = d3.scale.sqrt()
		.domain([0, 2408])// Valeurs min/max de la série de données (dépenses) min = 0 et max : 2407,4
		.range([0, 30]);// Taille des symboles proprotionnels

	// Fonction permettant de créer la carte des symboles propotionnels
	var dessinSP = function(geojson) { // Appel du fichier geojson
		// Nouvel objet pour les symboles
		svg.append("g")
		.attr("class", "cercle") // Type de symbole : des cercles
		.selectAll("circle")
		.data(geojson.features)
		.enter().append("circle")
		.attr("transform", function(d) { return "translate(" + path.centroid(d) + ")"; }) // Création de points aux centroides des objets du fichier geojson
		.attr("r", function(d) {
			d_dep = donnees_stat.filter(function (d2){ if(d.properties.Code_pays === d2.pays) return d2;});
			return radius(d_dep[(date-1996)].dep);
		}) // Variation de la taille des points en fonction de la racine carrée de la valeur
		.attr('stroke','black') // Couleur du contours
		.attr('fill', 'rgba(77, 77, 77, 0.2)') // Couleur des symboles
	};

	// -------------------------------- FIN DES SYMBOLES --------------------------------------------	

	// -------------------------------- DEBUT DES CONTOURS ------------------------------------------	
	//Fonction permettant de créer un masque au-dessus des cartes précédentes pour avoir le contours au-dessus, net et continu
	var dessinContours = function(geojson){
		var tip = d3.tip()
		.attr('class', 'd3-tip')
		.offset([-10, 0])
		.html(function(d) {
			d_tip = donnees_stat.filter(function (d2){ if(d.properties.Code_pays === d2.pays) return d2;});
			if ((d_tip[(date-1996)].dep === 0) && (d_tip[(date-1996)].evolution == null)){ // Si pas de données pour l'évolution 
				return "<span>"
				+d.properties.Nom_pays
				+"<hr ></span><i>Evolution du nombre de touristes entre " + (date-1) + " et " + date + " </i> : "
				+"n/a"
				+"<br><i>Dépense moyenne annuelle par touriste en " + date + "</i> : "
				+"n/a";
			} else if (d_tip[(date-1996)].dep === 0){ //s= Si pas de données pour la dépense
				return "<span>"
				+d.properties.Nom_pays
				+"<hr ></span><i>Evolution du nombre de touristes entre " +(date-1)+" et "+date+" </i> : "
				+Math.round(d_tip[(date-1996)].evolution * 10) / 10 + " %" // La valeur de l'évolution est arrondie à une décimale
				+"<br><i>Dépense moyenne annuelle par touriste en "+date+"</i> : "
				+"n/a";
			} else if (d_tip[(date-1996)].evo === null){ // Si pas de données pour les 2 variables
				return "<span>"
				+d.properties.Nom_pays
				+"<hr ></span><i>Evolution du nombre de touristes entre " +(date-1)+" et "+date+" </i> : "
				+"n/a"
				+"<br><i>Dépense moyenne annuelle par touriste en "+date+"</i> : "
				+Math.round(d_tip[(date-1996)].dep) + " $"; // La valeur de la dépense est arrondie à l'entier
			} else { // Si les 2 variables sont renseignées
				return "<span>"
				+d.properties.Nom_pays
				+"<hr ></span><i>Evolution du nombre de touristes entre " +(date-1)+" et "+date+" </i> : "
				+Math.round(d_tip[(date-1996)].evolution * 10) / 10 + " %"
				+"<br><i>Dépense moyenne annuelle par touriste en "+date+"</i> : "
				+Math.round(d_tip[(date-1996)].dep) + " $";
			}
		});
		svg.call(tip);
		
		// Nouvelle variable ayant les propriétés de svg  
		var continent3 = svg
		.append("g")
		.attr("code", "pays");

		// Nouvelle variable ayant les propriétés de continent3 
		var features3 = continent3
		.selectAll("path")
		.data(geojson.features);

		// Création des entités transparentes
		features3.enter()
			.append("path")
			.attr('class', 'pays')
			.attr('fill', 'rgba(255, 255, 255, 0)')
			.on('mouseover', function(d) { // Permet au survol de l'entité, d'afficher des données
				tip.show(d);
			})
			.on('mouseout',  function(d) {
				tip.hide(d);
			})
			.on('click', function(d,i){ // Mise à jour au clic
				pays_courant = d.properties.Code_pays;
	
				// Mise à jour de l'histogramme pays
				d3.select("#graphe").remove();
				d3.json('DATA.json', drawHisto);
	
				//Mise à jour des infos pays
				if (pays_courant != "AMS"){ // Si pas de pays sélectionné
					d3.select("#infoPays").remove();
					d3.json('DATA.json', infoPays);
				}
			})
			.attr('d', path);
	};

	// Fonction permettant de créer la carte en appellant successivement les différentes couches de dessin
	d3.json('Pays_donnees.geojson', function(geojson) { //Chargement du fichier geojson
		spinner.stop(); //Arrêt de l'indicateur de chargement
		dessinCh(geojson); //Choroplèthe
		dessinSP(geojson) //Symboles proportionnels
		dessinContours(geojson); //Contours des pays épais et survolables/cliquables
	});

	//Dessin des pays hors zone
	d3.json('Pays_hors_zone.geojson', function(geojson) {
		dessinPHZ(geojson);
	});

	// -------------------------------- FIN DES CONTOURS ----------------------------------------------

	// -------------------------------- DEBUT DATE COURANTE -----------------------------------------------
	// Affichage date courante en haut à droite de la carte
	svg.append("text")
	.attr("id","datemap")
	.attr ("x", width-430) // Positionnement en horizontal
	.attr("y",60) // Positionnement en vertical
	.style("text-anchor", "start")
	.attr ("font-size", "40px") // Taille de police
	.attr ("font-weight", "bold") // Casse de la police
	.attr ("font-family", "Helvetica") // Ecriture de police
	.attr("fill", "#696969") // Couleur de la police
	.text(date);
	// -------------------------------- FIN DATE COURANTE ------------------------------------------------

	// -------------------------------- DEBUT LEGENDE DEPENSES ----------------------------------------
	// Nouvelle variable pour la légende des cercles proportionnels
	var legend = svg.append("g")
	.attr("class", "legend")
	.attr("transform", "translate(" + (width - 145) + "," + (height - 590) + ")") // Situation des éléments dans la largeur et hauteur
	.selectAll("g")
	.data([500, 2000, 4000]) // Taille des cercles pour la légende (arbitraire couvrant l'étendue des données)
	.enter().append("g");

	// Nouvel objet cercle
	legend.append("circle")
	.attr("cy", function(d) { return -radius(d); })
	.attr("r", radius)
	.attr("fill", "none") // Aucun remplissage
	.attr("stroke", "black"); //Contours noirs

	// Etiquettes des cercles
	legend.append("text")
	.attr("y", function(d) { return -2 * radius(d); }) // Hauteur de l'interligne
	.attr("dy", "1.3em") // Positionnement des étiquettes en hauteur par rapport aux cercles
	.text(d3.format("d")) // Format des valeurs : le "d" peut être remplacé par "0.1s"
	.attr ("font-size", "10px") // Taille de police
	.attr ("font-weight", "normal") // Casse d'écriture
	.style("text-anchor", "middle"); // Ancrage du texte
 
	// Titre de la légende
	legend.append("text")
	.attr ("x", 0) // Situation du titre dans la largeur de la carte
	.attr("y",-90) // Situation du titre dans la hauteur de la carte
	.style("text-anchor", "middle")
	.attr("font-family", "Helvetica") // Famille de la police
	.attr ("font-size", "12px") // Taille de police
	.attr("fill", "#696969") // Couleur d'écriture
	.text("Dépense moyenne annuelle par touriste ($)"); // Titre 
		
	// -------------------------------- FIN LEGENDE DEPENSES ----------------------------------------------


	// -------------------------------- DEBUT LEGENDE EVOLUTION ------------------------------------------

	// Concernant la situation des rectangles de couleur
	var x_legend_e = width-232; // Emplacement du début du rectangle (horizontalement)
	var y_legend_e = height-580; // Emplacement du début du rectangle (verticalement)

	// Concernant les rectangles de couleurs
	var hautleg = 10; // Hauteur du rectangle de couleur : elle reste invariable, contrairement à sa largeur 
 
	// Nouvelle variable pour la légende de l'évolution du nombre d'arrivées 
	var legend_e = svg.append("g").attr("class", "legend");
  
	// Nouvel objet texte (titre de la légende)  
	legend_e.append("text")
	.attr ("x", x_legend_e+90) // Situation du titre dans la largeur de la carte
	.attr("y",y_legend_e+10) // situation du titre dans la hauteur de la carte
	.style("text-anchor", "middle")
	.attr("font-family", "Helvetica") // Famille de police
	.attr ("font-size", "12px") // Taille de police
	.attr("fill", "#696969") // Couleur d'écriture
	.text("Evolution du nombre de touristes (%)");  // Titre 
		
	// deuxième ligne de la légende 
	legend_e.append("text")
	.attr ("x", x_legend_e+90) // Situation du titre dans la largeur de la carte
	.attr("y",y_legend_e+24) // situation du titre dans la hauteur de la carte
	.style("text-anchor", "middle")
	.attr("font-family", "Helvetica") // Famille de police
	.attr ("font-size", "12px") // Taille de police
	.attr("fill", "#696969") // Couleur d'écriture
	.text("de l'année n-1 à l'année n sélectionnée");
		
	// boucle sur toutes les classes	

	for (i=0; i<=nbClasses-1; i++){
		legend_e.append("rect") //Ajouter un rectangle
		.attr("width", largeurClasse[i]) // Amplitude (étendue) de la classe stocké dans le tableau													
		.attr("height", hautleg) // Hauteur du rectangle (constante)
		.attr("x", x_legend_e+largClasseCumul[i]) // Emplacement du début du rectangle (horizontalement) tableau cumul
		.attr("y", y_legend_e+30) // Emplacement du début du rectangle (verticalement) 
		.attr("fill", couleurs[i+1]); // Attribution de la couleur du tableau
		
	}

	//rectangle si pas de données n/a
	legend_e.append("rect")
	.attr("width", 20)
	.attr("height", hautleg)
	.attr("x", x_legend_e+largClasseCumul[nbClasses-1]+largeurClasse[nbClasses-1]+20) 
	// Emplacement du début du rectangle selon la position et la largeur de la dernière classe
	.attr("y", y_legend_e+30)
	.attr("fill", couleurs[8]);
		
	legend_e.append("text")
	.text("n/a")
	.attr("x",x_legend_e+largClasseCumul[nbClasses-1]+largeurClasse[nbClasses-1]+30)
	.attr("y", y_legend_e+55)
	.attr("font-size","10px")
	.style("text-anchor", "middle")
	;
		
	// Nouvel axe x pour graduer la légende
	var xleg = d3.scale.linear()
	.domain([classes[0],classes[nbClasses]]) // Etendue des valeurs de classes
	.range([x_legend_e,x_legend_e+Math.abs(classes[0]-classes[nbClasses])]); // Donc 180 de largeur au total
		
	var xAxisleg = d3.svg.axis()
	.scale(xleg)
	.ticks(nbClasses) // Nombre de classes dans le dataset
	.tickFormat(d3.format("d")) // Format des valeurs (on peut aussi les mettre en pourcentage en remplaçant le "d" par "%"
	.tickValues(classes) // Valeurs des graduations
	.orient("bottom");

	// Nouvel objet texte : police d'écriture, emplacement, ... des étiquettes de l'axe		
	legend_e.append("g")
	.attr("class", "x axis")
	.attr("transform", "translate(0," +(y_legend_e+30+hautleg)+ ")")
	.attr("font-size", "10px")
	.attr("font-weight", "normal")
	.attr("fill", "black")
	.call(xAxisleg);
		
	// -------------------------------- FIN LEGENDE EVOLUTION -----------------------------------------


	// -------------------------------- DEBUT LEGENDE ECHELLE -----------------------------------------

	// Définition du titre de la légende
	legend.append("text")
	.attr ("x", 65) // Situation du titre dans la largeur de la carte
	.attr("y",+103) // Situation du titre dans la hauteur de la carte
	.style("text-anchor", "middle")
	.attr("font-family", "Helvetica")
	.attr ("font-size", "12px") // Taille de police
	.attr("fill", "#696969") // Couleur d'écriture
	.text("km"); 
		
	//Création de l'axe x de la légende
	var x = d3.scale.linear()
	.domain([0,1000])
	.range([0,83]);
		
	var xAxis = d3.svg.axis()
	.scale(x)
	.ticks(1) // Nombre de valeur dans le dataset
	.tickFormat(d3.format("d"))
	.orient("bottom");		
			
	legend.append("g")
	.attr("class", "x axis")
	.attr("transform", "translate("+(-43)+","+85+")")
	.attr ("font-size", "12px")
	.attr("font-family", "Helvetica")
	.attr("fill", "#696969")
	.call(xAxis);
		
	// -------------------------------- FIN LEGENDE ECHELLE ------------------------------------------
}